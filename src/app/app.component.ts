import { Component, ViewChild } from '@angular/core';
import { MbscFormOptions, MbscListviewOptions }  from '@mobiscroll/angular';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild('adeline', {static: false})
    adeline: any;
    @ViewChild('carl', {static: false})
    carl: any;
    @ViewChild('tinker', {static: false})
    tinker: any;
    @ViewChild('barry', {static: false})
    barry: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

    

    formSettings: MbscFormOptions = {
        theme: 'ios',
        themeVariant: 'light'
    };

    listviewSettings: MbscListviewOptions = {
        swipe: false,
        enhance: true
    };

    closeAll() {
        this.adeline.instance.hide();
        this.carl.instance.hide();
        this.tinker.instance.hide();
        this.barry.instance.hide();
    }

    toggleLast() {
        this.barry.instance.toggle();
    }
}



